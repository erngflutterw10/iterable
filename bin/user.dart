bool anyUserUnder18(Iterable<User> users){
  return users.any((user) => user.age < 18);
}

bool anyUserOver18(Iterable<User> users){
  return users.every((user) => user.age > 13);
}

Iterable<User> filterOutUnder21(Iterable<User> users) {
  return users.where((user) => user.age >= 21);
}

Iterable<User> findShortNamed(Iterable<User> users) {
  return users.where((user) => user.name.length <= 3);
}

Iterable<String> getNameAndAges(Iterable<User> users) {
  return users.map((user) => '${user.name} is ${user.age}');
}

void main() {
  var users = [
    User(name: 'AAAAA', age: 14),
    User(name: 'BBBBB', age: 18),
    User(name: 'CCC', age: 18),
    User(name: 'DDD', age: 21),
    User(name: 'E', age: 25)
  ];
  if(anyUserUnder18(users)){
    print('Have any user under 18');
  }
  if(anyUserOver18(users)){
    print('every user are over 13');
  }

  var AgeMoreThan21Users = filterOutUnder21(users);
  for (var user in AgeMoreThan21Users) {
    print(user.toString());
  }
  
  var shortNameUsers = findShortNamed(users);
  for (var user in shortNameUsers) {
    print(user.toString());
  }

  var numbersByTwo = const [1, -2, 3, 42].map((number) => number * 2);
  print('Numbers: $numbersByTwo');

  var nameAndAges = getNameAndAges(users);
  for (var user in nameAndAges) {
    print(user.toString());
  }
}

class User {
  String name;
  int age;
  User({required this.name, required this.age});
  @override
  String toString() {
    return '$name, $age';
  }
}
